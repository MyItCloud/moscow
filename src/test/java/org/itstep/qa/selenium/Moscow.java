package org.itstep.qa.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

public class Moscow {
    public static void main(String[] args){
        System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.navigate().to("http://hflabs.github.io/suggestions-demo/");
        WebElement webElement = driver.findElement(By.id("fullname" ));
        webElement.sendKeys("Сидоров Сидор Сидорович" + Keys.ENTER);
        webElement.click();
        webElement = driver.findElement(By.id("email"));
        webElement.sendKeys("sidor@mail.ru");
        webElement = driver.findElement(By.id("message"));
        webElement.sendKeys("Moscow never sleep");
        webElement = driver.findElement(By.xpath("//*[@id=\"feedback-form\"]/div[5]/button" ));
        webElement.click();
        webElement = driver.findElement(By.xpath("//*[@id=\"feedback-form\"]/div[5]/button" ));
        webElement = driver.findElement(By.xpath("//*[@id=\"feedback-message\"]" ));

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        webElement = driver.findElement(By.xpath("//*[@id=\"feedback-message\"]/h4" ));
        Assert.assertEquals(webElement.getText(), "Это не настоящее правительство :-(");
        webElement = driver.findElement(By.xpath("//*[@id=\"feedback-message\"]/p[1]" ));
        Assert.assertEquals(webElement.getText(), "К сожалению, мы не можем принять ваше обращение.\n" +
                "Но вы всегда можете отправить его через электронную приемную правительства Москвы.");
        webElement = driver.findElement(By.xpath("//*[@id=\"feedback-message\"]/p[2]/button" ));
        Assert.assertEquals(webElement.getText(), "Хорошо, я понял");
        webElement.click();

    }
}
